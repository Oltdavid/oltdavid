package DataDrivenTest;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import Utilities.Constans;

public class UsingExcell {
	private WebDriver driver;

	@BeforeClass
	public void setUp() throws Exception {
		driver = new ChromeDriver();

		// Maximize the browser's window
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		driver.get(Constans.URL);
		driver.findElement(By.xpath("html/body/div[2]/a")).click();
		// Tell the code about the location of Excel file
		ExcelUtility.setExcelFile(Constans.File_Path + Constans.File_Name, "FillData");
	}

	@DataProvider(name = "Data")
	public Object[][] dataProvider() {
		Object[][] testData = ExcelUtility.getTestData("Invalid_Data");
		return testData;
	}

	@Test(dataProvider = "Data")
	public void testUsingExcel(String facility, String reserved) throws Exception {

		// Enter facility
		driver.findElement(By.xpath(".//*[@id='reservationForm']/input[1]")).sendKeys(facility);

		// Enter start date
		driver.findElement(By.xpath("//form[@id='reservationForm']/input[@name='fromDate']")).sendKeys("2018", "	",
				"05", " ", "05");// start date

		// Enter end date
		driver.findElement(By.xpath("//form[@id='reservationForm']/input[@name='toDate']")).sendKeys("2018", "	", "05",
				" ", "25");
		// Enter reserved
		driver.findElement(By.xpath(".//*[@id='reservationForm']/input[4]")).sendKeys(reserved);

		// click place reservation
		driver.findElement(By.xpath(".//*[@id='submitReservation']")).click();

		driver.findElement(By.xpath(".//*[@id='reservationForm']/button[2]")).click();

		driver.findElement(By.xpath("html/body/div[2]/a")).click();

	}

	@AfterClass
	public void tearDown() throws Exception {
		driver.findElement(By.xpath(".//*[@id='reservationForm']/button[2]")).click();
		driver.findElement(By.xpath("html/body/div[1]/a")).click();

		// driver.quit();
	}
}