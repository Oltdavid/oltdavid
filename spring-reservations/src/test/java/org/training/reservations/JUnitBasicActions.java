package org.training.reservations;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;


public class JUnitBasicActions {
	WebDriver driver;

	String baseURL;

	@Before
	public void setUp() throws Exception {
		baseURL = "http://localhost/";
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

	}

	@Test
	public void test() {
		driver.get(baseURL);
		driver.findElement(By.xpath("html/body/div[2]/a")).click();
		System.out.println("Click on Make a reservation");
		driver.findElement(By.xpath(".//*[@id='reservationForm']/input[1]")).sendKeys("5");

	}

	@After
	public void tearDown() throws Exception {
		driver.quit();
	}

}
