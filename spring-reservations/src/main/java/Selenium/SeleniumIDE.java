package Selenium;

import java.util.concurrent.TimeUnit;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class SeleniumIDE {
	private WebDriver driver;
	private String baseUrl;

	@Before
	public void setUp() throws Exception {
		driver = new FirefoxDriver();
		baseUrl = "http://localhost/";
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	}

	@Test
	public void testSelenium() throws Exception {
		driver.get(baseUrl + "/");
		driver.findElement(By.linkText("Make a reservation")).click();
		driver.findElement(By.cssSelector("button[type=\"home\"]")).click();
		driver.findElement(By.linkText("List of reservations")).click();
		driver.findElement(By.cssSelector("button")).click();
		driver.findElement(By.cssSelector("button")).click();
		driver.findElement(By.cssSelector("button")).click();
		driver.findElement(By.cssSelector("button")).click();
	}

	@After
	public void tearDown() throws Exception {
		driver.quit();

	}

}
