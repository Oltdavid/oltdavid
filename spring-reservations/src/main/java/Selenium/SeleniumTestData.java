package Selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class SeleniumTestData {

	public static void main(String[] args) {
		String baseURL = "http://localhost/";
		WebDriver driver;
		
		System.setProperty("webdiver.chrome.driver", "C:\\STS\\Munka\\Chrome driver\\chromedriver.exe");
		driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get(baseURL);
		
		driver.findElement(By.xpath("/html//a[@href='/reservation.html']")).click();
		driver.findElement(By.xpath("//form[@id='reservationForm']/input[@name='resourceId']")).sendKeys("3");//Facility ID
	    driver.findElement(By.xpath("//form[@id='reservationForm']/input[@name='fromDate']")).sendKeys("2018","	","05"," ","05");//start date
	    driver.findElement(By.xpath("//form[@id='reservationForm']/input[@name='toDate']")).sendKeys("2018","	","05"," ","25");// end date
	    driver.findElement(By.xpath("//form[@id='reservationForm']/input[@name='owner']")).sendKeys("Test Name 1");//Reserved By
	    driver.findElement(By.xpath("/html//button[@id='submitReservation']")).click();// Place reservation
	    

	}

}
//